package commonUtils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;

public abstract class BaseTest implements IAutoConst
{
    static int passCount=0;
    static int failCount=0;
    public WebDriver driver;

    //before executing the test method , it executes the before method
    @BeforeMethod
    public void intialize()
    {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications"); //disables the notification pop up.
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get(URL);
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    //executes After method after executing the test method
    @AfterMethod
    public void closeApplication(ITestResult iTestResult)
    {
        String name=iTestResult.getName();
        int status=iTestResult.getStatus();
        if(status==1)
        {
          passCount++;
        }
        else
            failCount++;
        Reporter.log(name + "status : "+ status,true);
        driver.close();
        //test
    }

    //executes the below method at the end of all test methods' execution
    @AfterSuite
    public void print() throws Exception {
        try {
            String passCountS = Integer.toString(passCount);
            String failCountS = Integer.toString(failCount);
            FrameworkUtility.setXLData(REPORT_PATH, "ReportFile", 1, 0, passCountS);
            FrameworkUtility.setXLData(REPORT_PATH, "ReportFile", 1, 1, failCountS);
            Reporter.log("Pass count : " + passCount, true);
            Reporter.log("fail count : " + failCount, true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
