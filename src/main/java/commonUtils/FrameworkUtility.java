package commonUtils;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FrameworkUtility
{
    //Method to get the input data from the XL sheet
    public static String getXLData(String xlPath, String sheetName,int row,int column) throws IOException
    {
        String v = " ";
        try {

            FileInputStream inputStream = new FileInputStream(xlPath);
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            v = workbook.getSheet(sheetName).getRow(row).getCell(column).toString();
        }

        catch(Exception e)
        {
            e.printStackTrace();
        }

        return v;
    }

    //Method to get the total number of row count from the XL sheet
    public static int getXLRowCount(String xlPath,String sheet) throws Exception
    {
        int count=0;
        try
        {
            Workbook workbook=XSSFWorkbookFactory.create(new FileInputStream(xlPath));
            count=workbook.getSheet(sheet).getLastRowNum();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return count;
    }

    //Method to write the data in the XL sheet
    public static void setXLData(String xlPath,String sheet,int row, int column,String setValue) throws Exception
    {

        try
        {

            FileInputStream inputStream = new FileInputStream(xlPath);
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
           // Workbook workbook= XSSFWorkbookFactory.create(new FileInputStream(xlPath));
            {
                workbook.getSheet(sheet).getRow(row).getCell(column).setCellValue(setValue);
                workbook.write(new FileOutputStream(xlPath));
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    //Method to take screen shot
    public static void getScreenShot(WebDriver driver,String screenShotPath) throws Exception
    {
        try
        {
            TakesScreenshot takeScreenSHot=(TakesScreenshot)driver;
            File sourceFile=takeScreenSHot.getScreenshotAs(OutputType.FILE);
            File targetFile=new File(screenShotPath);
            FileUtils.copyFile(sourceFile,targetFile);;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}

