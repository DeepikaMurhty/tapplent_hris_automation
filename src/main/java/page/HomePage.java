package page;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class HomePage
{

    //Method to validate the actual and expected Home page title
    public void verifyHomePageIsDispalyed(WebDriver driver,String expectedTitle)
    {
        String actualTitle=driver.getTitle();
        Assert.assertEquals(actualTitle,expectedTitle);
    }
}
