package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    @FindBy(xpath = "//span[text()='Got it!']")
    private WebElement gotItButton;             //finds got it button

    @FindBy(name = "company")
    private WebElement companyNameTextBox;      //finds company text box

    @FindBy(name = "email")
    private WebElement userNameTextBox;     //finds username text box

    @FindBy(name = "password")
    private WebElement passWordTextBox;     //finds password text box

    @FindBy(xpath = "//span[text()='Sign In']")
    WebElement signInButton;            // finds sign In button

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void clickGotItButton() { gotItButton.click(); }     //method to click got it button

    //method to set company name
    public void setCompanyNameTextBox(String companyName) { companyNameTextBox.sendKeys(companyName); }

    //method to set user name
    public void setUserNameTextBox(String userName) {
        userNameTextBox.sendKeys(userName);
    }

    //method to set password
    public void setPassWordTextBox(String password) {
        passWordTextBox.sendKeys(password);
    }

    //method to click sign in button
    public void clickSignInButton()
    {
        signInButton.click();
    }


}
