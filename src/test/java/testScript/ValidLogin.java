package testScript;

import commonUtils.BaseTest;
import commonUtils.FrameworkUtility;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import page.HomePage;
import page.LoginPage;

import java.util.concurrent.TimeUnit;

public class ValidLogin extends BaseTest
{
    @Test

    //test case : valid login scenario : use valid company name , valid user name and valid password.

    public void testValidLogin() throws Exception {
        try {
            String companyName = FrameworkUtility.getXLData(XL_PATH, "validLogin", 1, 0);

            String userName = FrameworkUtility.getXLData(XL_PATH, "validLogin", 1, 1);

            String password = FrameworkUtility.getXLData(XL_PATH, "validLogin", 1, 2);

            String homePageTitle = FrameworkUtility.getXLData(XL_PATH, "validLogin", 1, 3);


            LoginPage loginPageObject = new LoginPage(driver);
            loginPageObject.clickGotItButton();
            loginPageObject.setCompanyNameTextBox(companyName);
            loginPageObject.setUserNameTextBox(userName);
            loginPageObject.setPassWordTextBox(password);
            HomePage homePageObject = new HomePage();
            homePageObject.verifyHomePageIsDispalyed(driver, homePageTitle);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}

